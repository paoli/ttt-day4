# Exploring Protein Features using InterPro


## Learning outcomes

* Trainees will be able to search and interpret InterPro pages and identify a range of protein relationships such as family, superfamily, sites and repeats for a given protein of interest.

## Learning objectives
* Learn what is InterPro.
* Distinguish between different InterPro entry types. 
* Search, retrieve and interpret information for a given protein in InterPro.
* Access information available on individual InterPro entry pages.
* Retrieve and interpret information provided in InterPro for proteins with structural models available in AF2DB.


## Learner profiles
James is working in Astra Zeneca, in the proteomics department, and he wants to use InterPro to help their team to identify potential drug targets in InterPro data.